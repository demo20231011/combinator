﻿using System.Linq;
using CombinatorLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
	[TestClass]
	public class UnitTestSimpleVersion
	{
		[TestMethod]
		public void Test1()
		{
			var a = new[] { 1, 1, 2, 1, 1, 0, 1 };
			var x = 2;
			var expected = new[] { (1, 1), (1, 1), (0, 2), };

			var actual = CombinatorSimple.EnumeratePairs(a, x);

			Assert.IsTrue(Utils.AreEqual(expected, actual));
		}
		[TestMethod]
		public void TestNonExistingSum()
		{
			var a = new[] { 1, 1, 2, 1, 1, 0, 1 };

			Assert.IsFalse(CombinatorSimple.EnumeratePairs(a, 0).Any());
			Assert.IsFalse(CombinatorSimple.EnumeratePairs(a, 4).Any());
		}
		[TestMethod]
		public void TestEmptyInput()
		{
			var pairs = CombinatorSimple.EnumeratePairs(new int[0], 2);

			Assert.IsFalse(pairs.Any());
		}
	}
}
