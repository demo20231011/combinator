﻿using System.Collections.Generic;
using System.Linq;
using static System.Math;

namespace UnitTestProject
{
	static class Utils
	{
		public static bool AreEqual(IEnumerable<(int, int)> pairs1, IEnumerable<(int, int)> pairs2)
		{
			var seq1 = pairs1.NormalizeAndOrder();
			var seq2 = pairs2.NormalizeAndOrder();

			return seq1.SequenceEqual(seq2);
		}
		private static IEnumerable<(int, int)> NormalizeAndOrder(this IEnumerable<(int, int)> pairs)
		{
			return pairs
				.Select(p => (Min(p.Item1, p.Item2), Max(p.Item1, p.Item2)))
				.OrderBy(p => p.Item1)
				.ThenBy(p => p.Item2);
		}
	}
}
