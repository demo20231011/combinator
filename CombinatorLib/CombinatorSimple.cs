﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CombinatorLib
{
	public static class CombinatorSimple
	{
		public static IEnumerable<(int, int)> EnumeratePairs(IEnumerable<int> source, int x)
		{
			if (source == null) throw new ArgumentNullException(nameof(source));

			var a = source.ToArray();

			var used = new bool[a.Length];

			for (int i = 0; i < a.Length - 1; i++)
			{
				for (int j = i + 1; j < a.Length; j++)
				{
					if (!used[i] && !used[j])
					{
						if (a[i] + a[j] == x)
						{
							used[i] = true;
							used[j] = true;

							yield return (a[i], a[j]);
						}
					}
				}
			}
		}
	}
}
