﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CombinatorLib
{
	public static class CombinatorOptimized
	{
		public static IEnumerable<(int, int)> EnumeratePairs(IEnumerable<int> source, int x)
		{
			if (source == null) throw new ArgumentNullException(nameof(source));

			// make copy of source to avoid side effects
			// the copy is ordered so we can use binary search later
			var a = source.OrderBy(i => i).ToArray();

			if (a.Length < 2) yield break;

			// quick check for impossible cases
			if (a[0] + a[1] > x) yield break;
			if (a[a.Length - 2] + a[a.Length - 1] < x) yield break;

			var indexMax = a.Length;

			for (int indexOfFirst = 0; indexOfFirst + 1 < indexMax; indexOfFirst++)
			{
				var firstValue = a[indexOfFirst];
				var secondValue = x - firstValue;

				var startIndex = indexOfFirst + 1;
				var length = indexMax - startIndex;

				if (a.BinarySearchLast(secondValue, startIndex, length, out var indexOfLastSecondValue))
				{
					yield return (firstValue, secondValue);

					indexMax = indexOfLastSecondValue; // shrink search range to exclude found element
				}
			}
		}

		/// <summary>
		/// <para>Searches for the last element that is equal to <paramref name="value"/>.</para>
		/// <para>Returns true if <paramref name="value"/> is found.</para>
		/// </summary>
		/// <param name="array"></param>
		/// <param name="value"></param>
		/// <param name="startIndex"></param>
		/// <param name="length"></param>
		/// <param name="indexOfValue"></param>
		/// <returns>True, if value is found, false - otherwise.</returns>
		private static bool BinarySearchLast(this int[] array, int value, int startIndex, int length, out int indexOfValue)
		{
			var size = length;
			var leftIndex = startIndex;

			while (size > 0)
			{
				var leftSize = size / 2;
				var middleIndex = leftIndex + leftSize;

				if (value < array[middleIndex])
				{
					size = leftSize;
				}
				else
				{
					leftIndex = middleIndex + 1;
					size -= leftSize + 1;
				}
			}

			var index = leftIndex - 1;

			if (index < startIndex || array[index] != value)
			{
				indexOfValue = -1;
				return false;
			}
			else
			{
				indexOfValue = index;
				return true;
			}
		}
	}
}
